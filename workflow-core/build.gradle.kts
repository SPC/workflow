import space.kscience.gradle.Maturity

plugins {
    id("space.kscience.gradle.mpp")
    `maven-publish`
}

val attributesVersion: String by rootProject.extra

kscience {
    jvm()
    js()
    native()
    useCoroutines()
    useSerialization()
    commonMain {
        api(spclibs.kotlinx.datetime)
        api("space.kscience:attributes-kt:$attributesVersion")
        api("com.benasher44:uuid:0.8.4")
    }

    jvmTest {
        implementation(spclibs.logback.classic)
    }
}


readme {
    maturity = Maturity.EXPERIMENTAL
}