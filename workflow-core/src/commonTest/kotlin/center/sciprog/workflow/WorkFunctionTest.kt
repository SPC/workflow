package center.sciprog.workflow

import kotlinx.coroutines.test.runTest
import kotlinx.datetime.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.time.Duration.Companion.days

internal class WorkFunctionTest {
    @Test
    fun testPayments() = runTest{
        val person = Person("test","Test Subject")

        val org = Organization("Master", "Master organization")

        val work = WorkList {
            val now = Clock.System.now()
            repeat(10) {
                transaction(org, person, 1000.rubles, time = now.minus((it*30).days))
            }
        }

        assertEquals(10000.0, Payments.inRubles(person).compute(work), 1.0)
    }
}