package center.sciprog.workflow

import kotlinx.serialization.Serializable

public interface Resource

public interface Currency

public data object Rubles : Currency

@Serializable
public data class Money(
    @Serializable(MoneyNumberSerializer::class) val amount: Number,
    val currency: Currency,
) : Resource

public val Number.rubles: Money get() = Money(this, Rubles)