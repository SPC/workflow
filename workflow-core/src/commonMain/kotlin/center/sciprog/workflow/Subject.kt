package center.sciprog.workflow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import space.kscience.attributes.AttributeContainer
import space.kscience.attributes.Attributes

public typealias SubjectId = String


public sealed interface Subject : AttributeContainer {
    public val id: SubjectId
}

public data class Person(
    override val id: SubjectId,
    public val name: String,
    override val attributes: Attributes = Attributes.EMPTY,
) : Subject

public data class Organization(
    override val id: SubjectId,
    public val name: String,
    override val attributes: Attributes = Attributes.EMPTY,
) : Subject

public interface SubjectProvider {
    public suspend fun ids(): Flow<SubjectId>
    public suspend fun provide(subjectId: SubjectId): Subject?
}

public class ListSubjectProvider(private val subjects: List<Subject>) : SubjectProvider {
    override suspend fun ids(): Flow<SubjectId> = subjects.asFlow().map { it.id }

    override suspend fun provide(subjectId: SubjectId): Subject? = subjects.find { it.id == subjectId }
}