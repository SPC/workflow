package center.sciprog.workflow

import kotlinx.coroutines.flow.Flow


public interface WorkBuilder {

    /**
     * Generate a unique event Id
     */
    public suspend fun generateId(): EventId

    /**
     * Add a single event to a builder
     */
    public suspend fun put(event: WorkEvent)

    /**
     *  Visit all events Remove or replace or don't change all events in the Work.
     *  * If the result of the [visitor] is the same as input, no changes are done.
     *  * If the result is a new event, the initial event is replaced
     *  * if the result is null, the initial event is removed
     */
    public suspend fun visit(visitor: (WorkEvent) -> WorkEvent?)
}

/**
 * Remove a single event with given ID from builder
 */
public suspend fun WorkBuilder.remove(id: EventId): Unit = visit { if (it.id == id) null else it }

public suspend fun WorkBuilder.putAll(events: Collection<WorkEvent>) {
    events.forEach {
        put(it)
    }
}

/**
 * Suspends until all [events] are put into this builder
 */
public suspend fun WorkBuilder.putAll(events: Flow<WorkEvent>) {
    events.collect {
        put(it)
    }
}

public suspend fun WorkBuilder.putAll(work: Work): Unit = putAll(work.flow())