package center.sciprog.workflow

import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

internal object MoneyNumberSerializer : KSerializer<Number> {
    //TODO replace by proper BigDecimal serializer

    private val proxySerializer = Double.serializer()

    override val descriptor: SerialDescriptor get() = proxySerializer.descriptor

    override fun deserialize(decoder: Decoder): Number =
        decoder.decodeSerializableValue(proxySerializer)

    override fun serialize(encoder: Encoder, value: Number) {
        encoder.encodeSerializableValue(proxySerializer, value.toDouble())
    }
}