package center.sciprog.workflow

import com.benasher44.uuid.uuid4
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow

private class WorkListBuilder(val idPrefix: String, val events: MutableList<WorkEvent>) : WorkBuilder {
    override suspend fun generateId(): EventId = idPrefix + "-" + uuid4().toString()

    override suspend fun put(event: WorkEvent) {
        events.add(event)
    }

    override suspend fun visit(visitor: (WorkEvent) -> WorkEvent?) {
        val iterator = events.listIterator()
        while (iterator.hasNext()) {
            val event = iterator.next()
            val newEvent = visitor(event)
            if (newEvent == null) {
                iterator.remove()
            } else if (newEvent != event) {
                iterator.set(newEvent)
            }
        }
    }
}


public class WorkList(private val events: List<WorkEvent>) : WorkWithBuilder<WorkList> {

    override fun flow(): Flow<WorkEvent> = events.asFlow()

    override suspend fun modified(block: suspend WorkBuilder.() -> Unit): WorkList {
        val modifiedEvents = ArrayList(events)
        val prefix = uuid4().leastSignificantBits.toString(16)
        WorkListBuilder(prefix, modifiedEvents).block()
        return WorkList((modifiedEvents))
    }
}

public suspend fun WorkList(block: suspend WorkBuilder.() -> Unit): WorkList {
    val events = mutableListOf<WorkEvent>()
    val prefix = uuid4().leastSignificantBits.toString(16)
    val builder = WorkListBuilder(prefix, events)
    builder.block()
    return WorkList(events)
}