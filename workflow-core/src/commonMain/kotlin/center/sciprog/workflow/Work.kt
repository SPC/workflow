package center.sciprog.workflow

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.merge
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDateTime
import space.kscience.attributes.AttributeContainer

public typealias WorkId = String

public typealias EventId = String

/**
 * A generic operation in a workflow
 */
public interface WorkEvent : AttributeContainer {
    /**
     * A unique ID for the operation
     */
    public val id: EventId

    /**
     * A localized time operation is attributed to
     */
    public val time: Instant
}

/**
 * A central API for all workflow computations
 */
public interface Work {

    public fun flow(): Flow<WorkEvent>

    public companion object {
        /**
         * Join several works into one
         */
        public fun join(vararg works: Work): Work = object : Work {
            override fun flow(): Flow<WorkEvent> = works.map { it.flow() }.merge()
        }
    }
}

/**
 * A work that can produce a modified work via [WorkBuilder]
 */
public interface WorkWithBuilder<W : Work> : Work {
    public suspend fun modified(block: suspend WorkBuilder.() -> Unit): W
}