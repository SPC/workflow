package center.sciprog.workflow

import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.fold
import kotlinx.datetime.Instant

public interface WorkFunction<T> {
    public val emptyBase: T
    public suspend fun compute(work: Work, base: T = emptyBase): T
}


public class Payments(
    public val subject: Subject,
    public val timeFrame: ClosedRange<Instant>,
    public val valueExtractor: (Money) -> Double,
    override val emptyBase: Double = 0.0,
) : WorkFunction<Double> {
    override suspend fun compute(work: Work, base: Double): Double {
        return work.flow().filterIsInstance<ExecutedTransaction>().filter {
            it.toSubject == subject && it.time in timeFrame
        }.fold(base) { acc, value -> acc + valueExtractor(value.amount) }
    }

    public companion object {
        public fun inRubles(
            subject: Subject,
            timeFrame: ClosedRange<Instant> = Instant.DISTANT_PAST..Instant.DISTANT_FUTURE,
        ): Payments = Payments(
            subject,
            timeFrame,
            { if (it.currency == Rubles) it.amount.toDouble() else TODO("No currency converter") }
        )

    }
}