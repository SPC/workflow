package center.sciprog.workflow

import kotlinx.datetime.*
import kotlinx.serialization.Serializable
import space.kscience.attributes.Attributes
import space.kscience.attributes.AttributesBuilder

/**
 * A money transaction
 */
public interface Transaction : WorkEvent {
    public val fromSubject: Subject
    public val toSubject: Subject
    public val amount: Money
}

/**
 * The transaction that has been executed. Some additional information could be encoded in [attributes]
 */
@Serializable
public data class ExecutedTransaction(
    override val id: EventId,
    override val fromSubject: Subject,
    override val toSubject: Subject,
    override val amount: Money,
    override val time: Instant,
    override val attributes: Attributes,
) : WorkEvent, Transaction

public suspend fun WorkBuilder.transaction(
    fromSubject: Subject,
    toSubject: Subject,
    amount: Money,
    time: Instant = Clock.System.now(),
    attributesBuilder: AttributesBuilder<ExecutedTransaction>.() -> Unit = {},
) {
    put(ExecutedTransaction(generateId(), fromSubject, toSubject, amount, time, Attributes(attributesBuilder)))
}

/**
 * The transaction that is planned, but not yet executed
 */
@Serializable
public data class PlannedTransaction(
    override val id: EventId,
    public val plannedOn: Instant,
    override val fromSubject: Subject,
    override val toSubject: Subject,
    override val amount: Money,
    override val time: Instant,
    override val attributes: Attributes,
) : WorkEvent, Transaction


public suspend fun WorkBuilder.plannedTransaction(
    fromSubject: Subject,
    toSubject: Subject,
    plannedOn: Instant,
    amount: Money,
    time: Instant = Clock.System.now(),
    attributesBuilder: AttributesBuilder<PlannedTransaction>.() -> Unit = {},
) {
    put(PlannedTransaction(generateId(), plannedOn, fromSubject, toSubject, amount, time, Attributes(attributesBuilder)))
}